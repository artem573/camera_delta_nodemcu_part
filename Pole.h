#ifndef Pole_h
#define Pole_h

#include "Aux_func.h"
#include "Point.h"
#include "StepperMotor.h"

#define TOL_LARM 0.1 // [m]

class Pole
{
  const double _R_SHEAVE;
  const double _L_SHEAVE;

  Point _POS;
  Point _POS_CARGO;
    
  double _l_arm;
  double _l_arm0;
  double _dldt;
  StepperMotor _motor;
	
public:
  
  Pole(double r_sheave, StepperMotor motor);

  void setPOS(Point POS);
  void setPOS_CARGO(Point POS_CARGO);

  void set_speed(double dldt);
  
  void move();
  const Point & pos() const;
  bool moved() const;
  void update();
  void updateLArm();
  double l_arm() const;

  void reset();
};
	
#endif // Pole_h
