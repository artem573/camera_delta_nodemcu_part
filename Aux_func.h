#ifndef Aux_func_h
#define Aux_func_h

double sqr(double a);

double sign(double a);

double max(double a, double b);
double min(double a, double b);

	
#endif // Aux_func_h
