#include "Pole.h"

Pole::Pole(double r_sheave, StepperMotor motor) :
  _R_SHEAVE(r_sheave),
  _L_SHEAVE(2.0 * 3.14 * r_sheave),
  _POS(0,0,0),
  _POS_CARGO(0,0,0),
  _l_arm(0),
  _l_arm0(_l_arm),
  _dldt(0),
  _motor(motor)
{}

void Pole::setPOS(Point POS)
{
  _POS.x() = POS.x();
  _POS.y() = POS.y();
  _POS.z() = POS.z();  
}

void Pole::setPOS_CARGO(Point POS_CARGO)
{
  _POS_CARGO.x() = POS_CARGO.x();
  _POS_CARGO.y() = POS_CARGO.y();
  _POS_CARGO.z() = POS_CARGO.z();  
}

void Pole::set_speed(double dldt)
{
  _dldt = dldt;
  
  bool dir = _dldt > 0;
  double dndt = fabs(_dldt) / _L_SHEAVE;
 
  _motor.set_direction(dir);
  _motor.set_speed(dndt);
}
	
void Pole::move()
{ 
  _motor.run(); 
  if (_motor.rotated()) { _l_arm += sign(_dldt) * _L_SHEAVE / _motor.steps_per_rev(); }
}

const Point & Pole::pos() const { return _POS; }

bool Pole::moved() const
{
  // TODO: remade condition to send data if moved
  if (fabs(_l_arm - _l_arm0) > TOL_LARM) { return true; }
  return false;
}

void Pole::update()
{
  _l_arm0 = _l_arm;
}

void Pole::updateLArm()
{
  _l_arm = sqrt(sqr(_POS_CARGO.x() - _POS.x()) +
                sqr(_POS_CARGO.y() - _POS.y()) +
                sqr(_POS_CARGO.z() - _POS.z()));            
}

double Pole::l_arm() const { return _l_arm; }

void Pole::reset()
{
  set_speed(0);
  _l_arm = sqrt(sqr(_POS_CARGO.x() - _POS.x()) +
                sqr(_POS_CARGO.y() - _POS.y()) +
                sqr(_POS_CARGO.z() - _POS.z()));
  _l_arm0 = _l_arm;
}
