#ifndef StepperMotor_h
#define StepperMotor_h

#include "Arduino.h"

class StepperMotor
{

private:

  const int _dir_pin;
  const int _stp_pin;
  const int _steps_per_rev;

  bool _dir;
  bool _state;
  bool _stepped;

  unsigned long _step_delay_ms;     // delay between steps, in ms, based on speed
  unsigned long _last_step_time_ms; // time stamp in ms of when the last step was taken

public:
  StepperMotor(int dir_pin, int stp_pin, int steps_per_rev);

  void set_speed(double speed_rev_per_sec);
  void set_direction(bool dir);
  bool dir() const;
  void run();
  bool rotated();
  int steps_per_rev() const;
};

#endif // StepperMotor_h
