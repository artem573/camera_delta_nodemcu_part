#include <cstring>
#include <ESP8266WiFi.h>

#include "StepperMotor.h"
#include "Pole.h"
#include "Point.h"


const char* WiFi_SSID = "bugoga5";
const char* WiFi_PASS = "123454321";

const char SERVICE_KEY[] = "#";

const int SERVER_PORT = 22000;
const IPAddress SERVER_IP(192,168,0,105);

Point POS(0, 0, 0);
Point POS_CARGO(0, 0, 0);

const int PIN_EN = 12;
const int PIN_DIR = 13;
const int PIN_STP = 14;

const double R_SHEAVE = 0.014/2;
const int STEPS_PER_REV = 200;

const char ID[] = "poleA";

const int INP_SIZE = 32;

WiFiClient client;
Pole pole(R_SHEAVE, StepperMotor(PIN_DIR, PIN_STP, STEPS_PER_REV));

void setup ()
{
  pinMode(PIN_EN, OUTPUT);
  digitalWrite(PIN_EN, LOW);

  Serial.begin(9600);

  delay(2000);

  WiFi.begin(WiFi_SSID, WiFi_PASS);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.println("Connecting...");
  }
  Serial.print("Connected to WiFi. IP:");
  Serial.println(WiFi.localIP());
}

void loop ()
{
  if (client.connect(SERVER_IP, SERVER_PORT))
  {
    Serial.print("Connected to ");
    Serial.println(SERVER_IP);
    client.print(ID);

    // Service message, now it's only the POS and POS_CARGO_0 info
    bool set = false;
    while (client.connected())
    {
      int size;
      
      while ((size = client.available()) > 0)
      {
        char msg[size];
        client.read((uint8_t*)msg, sizeof(msg));

        if (msg[0] == SERVICE_KEY[0])
        {
          Serial.print("initial: ");
          Serial.println(msg);
          char* coord = strtok(msg, SERVICE_KEY);
          int N = 6;
          double coords[N];
          int ni = 0;
          while (coord)
          {
            coords[ni++] = atof(coord);
            coord = strtok(NULL, SERVICE_KEY);
          }
          pole.setPOS(Point(coords[0], coords[1], coords[2]));
          pole.setPOS_CARGO(Point(coords[3], coords[4], coords[5]));
          pole.updateLArm();
          pole.update();
          set = true;
        }
      }
    }

    // An ordinary message with d(l_arm) value
    while (client.connected())
    {
      int size;
      while ((size = client.available()) > 0)
      {
        char msg[size];
        client.read((uint8_t*)msg, sizeof(msg));
        // Serial.println(msg);
        double val = atof(msg);
        // Serial.println(val);
        pole.set_speed(val);
      }

      if (pole.moved())
      {
        char msg[INP_SIZE];
        snprintf(msg, sizeof(msg), "%f\\n", pole.l_arm());
        client.write(msg);
        pole.update();
      }
            
      pole.move();
    }

    client.stop();
    Serial.println();
    Serial.print("Disconnected from ");
    Serial.println(SERVER_IP);

    pole.reset();
  }
}
