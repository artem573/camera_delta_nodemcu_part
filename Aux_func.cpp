#include "Aux_func.h"

double sqr(double a) { return a * a; }

double sign(double a) { return (a > 0 ? 1 : -1); }

double max(double a, double b) { return a > b ? a : b; }

double min(double a, double b) { return a < b ? a : b; }
