#include "StepperMotor.h"

StepperMotor::StepperMotor(int dir_pin, int stp_pin, int steps_per_rev):
  _dir_pin(dir_pin),
  _stp_pin(stp_pin),
  _steps_per_rev(steps_per_rev),
  _dir(0),
  _state(0),
  _stepped(0),
  _step_delay_ms(UINT32_MAX),
  _last_step_time_ms(millis())
{
  // setup the pins on the microcontroller:
  pinMode(_dir_pin, OUTPUT);
  pinMode(_stp_pin, OUTPUT);
}

// Sets the speed in revs per minute
void StepperMotor::set_speed(double speed_rps)
{
  const double steps_per_s = _steps_per_rev * (speed_rps + 1e-07);
  // const unsigned long sec_to_musec = 1000L * 1000L;
  const double s_to_ms = 1e3;

  if (steps_per_s > s_to_ms / UINT32_MAX) { _step_delay_ms = s_to_ms / steps_per_s; }
  else { _step_delay_ms = UINT32_MAX; }
}

void StepperMotor::set_direction(boolean dir) 
{ 
  if (_dir != dir)
  {
    _dir = dir;
    digitalWrite(_dir_pin, _dir);
  }
}

bool StepperMotor::dir() const { return _dir; }

void StepperMotor::run()
{
  const unsigned long now_ms = millis();
  const unsigned long delta_ms = now_ms - _last_step_time_ms;
  // move only if the appropriate delay has passed:
  // if (delta > 1.5 * _step_delay) 
  // { 
  //   Serial.print("Missed step "); 
  //   Serial.println(_dir_pin);    
  // }
  
  if ((delta_ms >= _step_delay_ms / 2) && (_state == 0)) 
  { 
    digitalWrite(_stp_pin, HIGH); 
    _state = 1;
  }
  else if ((delta_ms >= _step_delay_ms) && (_state == 1)) 
  { 
    digitalWrite(_stp_pin, LOW); 
    _state = 0;
    _last_step_time_ms = now_ms;
    _stepped = 1;
  }
}

bool StepperMotor::rotated()
{
  if (_stepped) 
  {
    _stepped = 0;
    return 1;
  }
  
  return 0;    
}

int StepperMotor::steps_per_rev() const { return _steps_per_rev; }
