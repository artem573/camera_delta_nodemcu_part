#ifndef Point_h
#define Point_h

class Point
{
  double _x;
  double _y;
  double _z;
	
public:

  Point(double x, double y, double z) :
    _x(x),
    _y(y),
    _z(z)
  { }
	
  const double x() const { return _x; }
  const double y() const { return _y; }
  const double z() const { return _z; }
	
  double & x() { return _x; }
  double & y() { return _y; }
  double & z() { return _z; }
};
	
#endif // Point_h
